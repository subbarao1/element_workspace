package com.admin.integration.manage;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.admin.integration.aml.AmlPage;
import com.admin.integration.common.Login;
import com.admin.integration.common.Util;

public class PolicyEnforcementTest {
	WebDriver driver;
	String companyname;
	String jurisdiction;
	String actual=null,expected=null;
	String temp=null;
	ManagePage manage;

	
	@BeforeClass
	public void startup() {
		final Login login=new Login(driver);
		driver = login.setup();
	}
	@Test(priority=2)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=3)
	public void manage() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickPolicyEnforcement();
		Thread.sleep(6000);
	}
	@Test(priority=4)
	public void TS_001_12() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.switchingWindow(1);
		pageLoadComplete();
		manage.clickDesign();
		manage.clickAlertGeneration();
		Thread.sleep(6000);
		manage.clickAlertRules();
		Thread.sleep(6000);
		manage.clickSource();
		Thread.sleep(6000);
		//manage.clickRandoLine();
		//System.out.println("text from the line: "+manage.getTextFromSource());
		Assert.assertTrue(manage.getTextFromSource().contains("function java.util.List addDescriptionOfAlert(String description,java.util.List descriptioList){"));
		System.out.println("Test case pass if java code contains this line in 31st row, otherwise fail. !! \n"
				+ "function java.util.List addDescriptionOfAlert(String description,java.util.List descriptioList){"
				+ "!! test case passed because this line of code prasented in the expected line only !!");
	}
	public void pageLoadComplete() {
		new WebDriverWait(driver, 10000).until(
				webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	}
	@SuppressWarnings("resource")
	@Test(enabled=false, priority=5)
	public void TS_001_11() throws IOException{

		Util util=new Util();
		System.out.println(util.getFilePath()+util.getFileName());
		System.out.println();
		XSSFWorkbook srcBook = new XSSFWorkbook(util.getDownloadsPath() + util.getExcelFilePath());     
		XSSFSheet sourceSheet = srcBook.getSheetAt(0);
		int rownum=12;
		XSSFRow sourceRow = sourceSheet.getRow(rownum);
		XSSFCell cell1=sourceRow.getCell(0);
		XSSFCell cell2=sourceRow.getCell(24);
		//XSSFCell cell3=sourceRow.getCell(2);
		//System.out.println(cell1);
		//System.out.println(cell2);
		//System.out.println(cell3);
		expected="Credit Card used to buy luxury item at High Risk Business";
		actual=cell1.toString();
		Assert.assertEquals(actual, expected);
		System.out.println("actual and expected are same like : "+cell1+" !! So test case passed !!");

		expected="70.0";
		actual=cell2.toString();
		Assert.assertEquals(actual, expected);
		System.out.println("actual and expected are same like : "+cell2+" !! So test case passed !!");
		
	}
	@Test(enabled=false,priority=6)
	public void serverStatus() throws InterruptedException {
		manage.clickHome();
		Thread.sleep(5000);
		manage.clickServers();
		Thread.sleep(5000);
		//System.out.println(manage.getServerStartStatus());
		Thread.sleep(5000);
		Assert.assertTrue(manage.getServerStartStatus().contains("disabled active"));
		System.out.println("Both the servers are up and running");

	}
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}

}
