package com.admin.integration.manage;

import java.awt.AWTException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.admin.integration.aml.AmlPage;
import com.admin.integration.common.Login;

public class BigDataSciencePlatformTest {
	WebDriver driver;
	//String companyname;
	//String jurisdiction;
	String actual=null,expected=null;
	ManagePage manage;

	
	@BeforeClass
	public void startup() {
		final Login login=new Login(driver);
		driver = login.setup();
	}
	@Test(priority=2)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=3)
	public void TS_001_13() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickBDSP();
		manage.clickcreateNewWorkflow();
		manage.typeWorkFlowName();
		manage.clickCreate();
		
		expected="SOURCES";
		actual=manage.getBDSPSource();
		Assert.assertEquals(actual, expected);
		expected="OPERATIONS";
		actual=manage.getBDSPOperations();
		Assert.assertEquals(actual, expected);
		expected="ANALYTIC";
		actual=manage.getBDSPAnalytic();
		Assert.assertEquals(actual, expected);
		expected="GIS SENSITIVITY";
		actual=manage.getBDSPGisSensivity();
		Assert.assertEquals(actual, expected);
		expected="IOT";
		actual=manage.getBDSPiot();
		Assert.assertEquals(actual, expected);
		expected="SINKS";
		actual=manage.getBDSPSinks();
		Assert.assertEquals(actual, expected);
		expected="DOMAIN";
		actual=manage.getBDSPDomain();
		Assert.assertEquals(actual, expected);
		expected="INDUSTRIES";
		actual=manage.getBDSPIndustries();
		Assert.assertEquals(actual, expected);
		expected="GRAPHS";
		actual=manage.getBDSPGraphs();
		Assert.assertEquals(actual, expected);
		expected="8";
		actual=String.valueOf(manage.bdspRightPanelicons());
		Assert.assertEquals(actual, expected);
		
		System.out.println("Total right panel icons are : "+actual);
		System.out.println("1 "+manage.getBDSPSource());
		System.out.println("2 "+manage.getBDSPOperations());
		System.out.println("3 "+manage.getBDSPAnalytic());
		System.out.println("4 "+manage.getBDSPGisSensivity());
		System.out.println("5 "+manage.getBDSPiot());
		System.out.println("6 "+manage.getBDSPSinks());
		System.out.println("7 "+manage.getBDSPDomain());
		System.out.println("8 "+manage.getBDSPIndustries());
		System.out.println("9 "+manage.getBDSPGraphs());
	}
	@Test(priority=3)
	public void TS_001_14() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.getBDSPSourcehdfs();
		manage.getBDSPSinksElasticSearch();
		//
		manage.clickElastic();
		manage.clickonIndex();
		manage.typeIndexName();
		manage.clickSave();
		
		manage.clickHdfs();
		manage.clcikhdfsDataSource();
		manage.clcikhdfsChoosedataFilesExplore();
		manage.clickhdfsCDFExploreFirstLink();
		manage.clickhdfsCDFName();
		manage.clickhdfsCDFDone();
		manage.clickhdfsDefineSchema();
		manage.clickhdfsDSSelectAll();
		manage.clickhdfsDSDone();
		manage.clickhdfsDSSave();
		manage.clickSaveWorkflow();
		manage.getBDSPSAveSuccessMessage();
		System.out.println(manage.getBDSPSAveSuccessMessage());
	}
	@Test(priority=4)
	public void drag() throws InterruptedException, AWTException {
		manage = new ManagePage(driver);
		manage.dragConnection();
		
	}
	
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
