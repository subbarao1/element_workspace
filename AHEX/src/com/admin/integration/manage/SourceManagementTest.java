package com.admin.integration.manage;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.admin.integration.aml.AmlPage;
import com.admin.integration.common.Login;

public class SourceManagementTest {
	WebDriver driver;
	//String companyname;
	//String jurisdiction;
	String actual=null,expected=null;
	ManagePage manage;

	
	@BeforeClass
	public void startup() {
		final Login login=new Login(driver);
		driver = login.setup();
	}
	@Test(priority=2)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=3)
	public void TS_001_01() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickSourceManagement();
		
		Assert.assertTrue(manage.getSMGeneralActive());
		System.out.println("By default General is active");
		System.out.println("General sources are : "+manage.getSMResources());
		Assert.assertTrue(Integer.parseInt(manage.getSMResources())>5);
	}
	@Test(priority=4)
	public void TS_001_02() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickSMNews();
		System.out.println("News sources are : "+manage.getSMResources());
		Assert.assertTrue(Integer.parseInt(manage.getSMResources())>5);
	}
	@Test(priority=5)
	public void TS_001_03() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickSMIndex();
		System.out.println("Index sources are : "+manage.getSMResources());
		Assert.assertTrue(Integer.parseInt(manage.getSMResources())>5);
	}
	@Test(priority=6)
	public void TS_001_05() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickSMGeneral();
		manage.typeSource();
		Assert.assertTrue(manage.searchSourceText());
	}
	@Test(priority=7)
	public void TS_001_06() throws InterruptedException {
		manage = new ManagePage(driver);
		
		manage.clickedit();
		manage.enterSourceName();
		manage.clicksourceUpdate();
		
	}
	@Test(priority=8)
	public void TS_001_04() throws InterruptedException {
		manage = new ManagePage(driver);
		manage.clickAddSourceBtn();
		manage.typeSourceName();
		manage.typeSourceLink();
		manage.clickIndustrySelect();
		manage.typeIndustrySelect();
		manage.clickIndustrySelectflink();
		manage.clickJurisdictionSelect();
		manage.typeJurisdictionSelect();
		manage.clickJurisdictionSelectFLink();
		manage.selectCategory();
		manage.clickAddSource();
	}
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
