package com.admin.integration.act;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.openqa.selenium.WebDriver;

import com.admin.integration.act.AuditTrailPage;
import com.admin.integration.aml.AmlPage;
import com.admin.integration.common.Login;

public class AuditTrailTest {
	WebDriver driver;
	String actual="", expected="";

	@BeforeClass
	public void startup() {
		final Login login=new Login(driver);
		driver = login.setup();
	}

	@Test(priority=1)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=2)
	public void TS_003_01() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickAudittrail();
		//System.out.println("tes :: "+audit.getLogActivityText());
		expected= "RECENT ACTIVITY";
		actual = audit.getLogActivityText();
		Assert.assertEquals(actual, expected);
	}
	@Test(priority=3)
	public void TS_003_02() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clicklogSearch();
		Assert.assertTrue(audit.recentActivityText("SEARCHED FOR AN ENTITY"));
	}
	@Test(priority=4)
	public void TS_003_03() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickActivity();
		Assert.assertTrue(audit.recentActivityText("COMPLIANCE WIDGET REVIEW"));
	}
	@Test(priority=5)
	public void TS_003_04() throws InterruptedException {
		final AuditTrailPage audit=new AuditTrailPage(driver);
		audit.clickviewby();
	}
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
