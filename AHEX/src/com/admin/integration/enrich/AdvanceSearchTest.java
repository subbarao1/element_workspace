package com.admin.integration.enrich;

import org.testng.annotations.Test;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.admin.integration.aml.AmlPage;
import com.admin.integration.common.Login;
import com.admin.integration.common.Util;
import com.admin.integration.shared.SharedPage;

public class AdvanceSearchTest {
	WebDriver driver;
	String url;
	Util util;
	String username;
	String password;
	String companyname;
	String jurisdiction;
	String actual="",expected="";


	/**
	 * Sets the up value.
	 */
	@BeforeClass public void setUp() {
		final Login login=new Login(driver);
		driver = login.setup();
	}


	@Test(priority=2)
	public void domain() {
		final AmlPage aml=new AmlPage(driver);
		aml.clickAML();
	}
	@Test(priority=3)
	public void process() {
		final SharedPage spage =new SharedPage(driver);
		spage.clickAdvanceSearch();
		//48 Demo Sanity Test
	}

	@Test(enabled=false)
	public void TS_004_08() throws InterruptedException {
		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickEntityType();
		enrich.clickEntityCompany();
		enrich.enterCompanyname(companyname);
		enrich.enterjurisdiction(jurisdiction);
		enrich.clickUpdate();
		enrich.clickSearch();
		//clickFirstResult();
		//47 Demo Sanity Test
	}
	@Test(priority=6)
	public void clickFirstResult() throws InterruptedException {
		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickFirstLink();
	}
	@Test(priority=4)
	public void TS_004_10() throws InterruptedException {
		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickSavedSearch();
		Thread.sleep(3000);
		//System.out.println("save search reuslts are : "+enrich.getSearchResultText());
		enrich.clickSearchCancel();
		//49 Demo Sanity Test
	}
	@Test(priority=5)
	public void TS_004_09() throws InterruptedException {
		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickRecentSearch();
		Thread.sleep(3000);
		enrich.getSearchResultText();
		enrich.clickSaveSearch();
		Thread.sleep(3000);
		//System.out.println(enrich.getSaveSuccessMessage());
		//enrich.clickSearchCancel();
		//48 Demo Sanity Test
	}


	@Test(priority=7)
	public void switchToNewWindow() throws InterruptedException {
		final EnrichPage enrich= new EnrichPage(driver);
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		Thread.sleep(25000);
		enrich.clickFinancial();
	}
	@Test(enabled=false)
	public void dataNotFoundCount() throws InterruptedException {
		Thread.sleep(15000);

		final EnrichPage enrich= new EnrichPage(driver);

		enrich.getCompanyName();
		int i = enrich.checkDataNotFound("data");
		System.out.println("total data not founds are  : "+ i);
		i = enrich.checkDataNotFound("loading..");
		System.out.println("total loadings are  : "+ i);
	}

	@Test(priority=8)
	public void TS_004_11() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickOverview();
		Thread.sleep(5000);

		expected="Who Are We";
		actual=enrich.getTextWhoAreWe();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Leadership";
		actual=enrich.getTextLeadership();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Stock Performance";
		actual=enrich.getTextStockPerformance();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Visual Link Analysis";
		actual=enrich.getTextVisualLinkAnalysis();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Associated Companies";
		actual=enrich.getTextAssociatedCompanies();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="CONTACT ADDRESS";
		actual=enrich.getTextContactAddress();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RISK SCORE";
		actual=enrich.getTextOverviewRiskScore();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RISK RATINGS";
		actual=enrich.getTextRiskRating();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RELATED ENTITIES";
		actual=enrich.getTextRelatedEntities();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="ASSOCIATED LOCATIONS";
		actual=enrich.getTextAssociatedLocations();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="LATEST NEWS";
		actual=enrich.getTextLatestNews();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="ASSOCIATED INDUSTRIES";
		actual=enrich.getTextAssociatedIndustries();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="DIGITAL FOOTPRINTS";
		actual=enrich.getTextDigitalFootPrint();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		//50 Demo Sanity Test
	}
	@Test(priority=9)
	public void TS_004_12() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickCompliance();
		Thread.sleep(5000);

		expected="Company Information";
		actual=enrich.getTextCompanyInformation();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Company Details";
		actual=enrich.getTextCompanyDetails();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Company Identifiers";
		actual=enrich.getTextCompanyIdentifiers();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Ownership Configurations:";
		actual=enrich.getTextOwnershipConfigurations();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Ownership Structure ";
		actual=enrich.getTextOwnershipStructure();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Screening Configurations:";
		actual=enrich.getTextScreeningConfigurations();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Screening Results ";
		actual=enrich.getTextScreeningResult();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="COUNTRIES OF OPERATION";
		actual=enrich.getTextCountriesOfOperation();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="ASSOCIATED DOCUMENTS";
		actual=enrich.getTextAssociatedDocuments();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		//51 Demo Sanity Test
	}
	@Test(priority=10)
	public void TS_004_13() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickLeadership();
		Thread.sleep(5000);

		expected="Key Executive";
		actual=enrich.getTextKeyExecutive();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Board of Directors";
		actual=enrich.getTextBoardofDirectors();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Committee";
		actual=enrich.getTextCommittee();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RISK SCORE";
		actual=enrich.getTextLeadershipRiskScore();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RATING TREND";
		actual=enrich.getTextRatingTrend();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="TOP INFLUENCERS";
		actual=enrich.getTextTOPINFLUENCERS();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="People Also Visited";
		actual=enrich.getTextPeopleAlsoVisited();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Near By Companies";
		actual=enrich.getTextNearByCompanies();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Company Members";
		actual=enrich.getTextCompanyMembers();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Positive Business Outlook";
		actual=enrich.getTextPositiveBusinessOutlook();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="CAREER OPPORTUNITIES";
		actual=enrich.getTextCareerOpportunities();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Leadership Clusters";
		actual=enrich.getTextLeadershipClusters();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Associated Persons";
		actual=enrich.getTextAssociatedPersons();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		//52 Demo Sanity Test
	}

	@Test(priority=11)
	public void TS_004_14() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickFinancial();
		Thread.sleep(5000);

		expected="RISK SCORE";
		actual=enrich.getTextFinanaceRiskScore();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="TOTAL REVENUES VS YEAR";
		actual=enrich.getTextTotalRevenuesVsYear();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="SHAREHOLDERS TYPE";
		actual=enrich.getTextShareholdersType();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Stock History";
		actual=enrich.getTextStockHistory();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Net Cash Flow - Operating Activities";
		actual=enrich.getTextNetCashFlow();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="Balance Sheet";
		actual=enrich.getTextBalanceSheet();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="INCOME STATEMENT";
		actual=enrich.getTextIncomeStatement();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="TOP HOLDERS";
		actual=enrich.getTextTopHolders();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="SHARE HOLDER NETWORK";
		actual=enrich.getTextShareHolderNetwork();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		//53 Demo Sanity Test
	}
	@Test(priority=12)
	public void TS_004_15() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickRiskAlerts();
		Thread.sleep(5000);

		expected="Alert Timeline";
		actual=enrich.getTextAlertTimeline();
		//System.out.println(actual);
		Assert.assertTrue(actual.contains(expected));

		expected="RISK SCORE";
		actual=enrich.getTextRiskRiskScore();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="POLITICAL DONATIONS TREND";
		actual=enrich.getTextPoliticalDonationsTrend();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="PEP ALERTS";
		actual=enrich.getTextPepAlerts();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="FRAUD ALERTS";
		actual=enrich.getTextFraudAlerts();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="CYBER ALERTS";
		actual=enrich.getTextCyberAlerts();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="POLITICAL RATIO";
		actual=enrich.getTextPoliticalRatio();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="SANCTIONED LIST";
		actual=enrich.getTextSanctionedList();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="NEWS";
		actual=enrich.getTextNews();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="PEP RELATIONSHIP CHART";
		actual=enrich.getTextPepRelationshipChart();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RISK RATIO";
		actual=enrich.getTextRiskRatio();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="INTERLOCKS";
		actual=enrich.getTextInterlocks();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="SUPPLIER'S BLACK LIST";
		actual=enrich.getTextSuppliersBlackList();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="INDUSTRIAL SECURITY INCIDENTS";
		actual=enrich.getTextIndustrialSecurityIncidents();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="CRIMINAL RECORDS";
		actual=enrich.getTextCriminalRecords();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());	

		expected="DARK WEB";
		actual=enrich.getTextDarkWeb();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());	
		//54 Demo Sanity Test
	}

	@Test(priority=13)
	public void TS_004_17() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);
		enrich.clickLatestNews();
		Thread.sleep(5000);

		expected="Overall Sentiment";
		actual=enrich.getTextOverallSentiment();
		Assert.assertTrue(actual.contains(expected));

		expected="RISK SCORE";
		actual=enrich.getTextNewsRiskScore();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="GENERAL SENTIMENTS";
		actual=enrich.getTextGeneralSentiments();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="LATEST NEWS";
		actual=enrich.getTextNewsLatestNews();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="PRESS RELEASES";
		actual=enrich.getTextPressReleases();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="POPULAR TAGS";
		actual=enrich.getTextPopularTags();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RECENT EVENTS";
		actual=enrich.getTextRecentEvents();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="RELATIONSHIP CHARTS BY NEWS";
		actual=enrich.getTextRelationshipChartsByNews();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

		expected="NEWS LOCATIONS";
		actual=enrich.getTextNewsLocations();
		Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		//56 Demo Sanity Test
	}
	@Test(priority=14)
	public void TS_004_16() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);

		if(enrich.totalEntityTabs() <= 6){
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		}else{

			enrich.clickThreatsIntelligence();
			Thread.sleep(5000);

			expected="Peer Group";
			actual=enrich.getTextPeerGroup();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Similar Companies";
			actual=enrich.getTextsimilarcompanies();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Risk Score";
			actual=enrich.getTextThreatsriskscore();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="By Top-Countries";
			actual=enrich.getTextbytopcountries();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="By Top Technologies";
			actual=enrich.getTextbytoptechnologies();
			System.out.println("??"+actual);
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected="Cyber Security";
			actual=enrich.getTextcybersecurity();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Select Location";
			actual=enrich.getTextselectlocation();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected="Select Industry";
			actual=enrich.getTextselectindustry();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected="displaying :";
			actual=enrich.getTextdisplaying();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Tag Cloud";
			actual=enrich.getTexttagcloud();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Threat Timeline";
			actual=enrich.getTextthreattimeline();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			expected="Incidents Classification By Industry";
			actual=enrich.getTextIncidentsClassificationByIndustry();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Incidents Classification";
			actual=enrich.getTextincidentsclassification();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			expected="Relationships";
			actual=enrich.getTextrelationships();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		}
	}
	@Test(priority=15)
	public void TS_004_18() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);

		if(enrich.totalEntityTabs() <= 6){
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		}else{

			enrich.clickSocialMedia();
			Thread.sleep(5000);

			expected="Activity Feed";
			actual=enrich.getTextActivityFeed();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			
			expected="Recent Tweets";
			actual=enrich.getTextRecentTweets();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="Instagram Posts";
			actual=enrich.getTextInstagramPosts();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="Facebook Social Feeds";
			actual=enrich.getTextFacebookSocialFeeds();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="Recent Linkedin Post";
			actual=enrich.getTextRecentLinkedinPost();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());

			expected="Google Plus Post";
			actual=enrich.getTextGooglePlusPost();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="Live Feed";
			actual=enrich.getTextLiveFeed();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="RISK SCORE";
			actual=enrich.getTextSocialMediaRISKSCORE();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="ACTIVITY LOCATIONS";
			actual=enrich.getTextACTIVITYLOCATIONS();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="TWITTER TAG WORDS";
			actual=enrich.getTextTWITTERTAGWORDS();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
			
			expected="INTERACTION RATIO";
			actual=enrich.getTextINTERACTIONRATIO();
			Assert.assertEquals(actual.toLowerCase(), expected.toLowerCase());
		}
	}
	@Test(priority=16)
	public void TS_004_19() throws InterruptedException {

		final EnrichPage enrich= new EnrichPage(driver);

		if(enrich.totalEntityTabs() <= 6){
			throw new SkipException("Skipping because Manage --> System Settings, selected other than the Complance ");
		}else{

			enrich.clickMedia();
			Thread.sleep(5000);

			expected="Images";
			actual=enrich.getTextImages();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
			
			expected="Risk Score";
			actual=enrich.getTextMediaRiskScore();
			Assert.assertTrue(actual.toLowerCase().contains(expected.toLowerCase()));
		}
	}
	@AfterClass
	public void endup() {
		driver.close();
		driver.quit();
	}
}
